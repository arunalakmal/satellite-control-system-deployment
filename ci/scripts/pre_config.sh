#!/bin/bash

wget https://github.com/mikefarah/yq/releases/download/v4.3.2/yq_linux_amd64
mv yq_linux_amd64 yq
chmod +x yq
mv yq /usr/local/bin/

export TAG=$(jq -r '.image_tag' versions.json)
export ACTIVE_PROD=$(jq -r '.active_production' versions.json)