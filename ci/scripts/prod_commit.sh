#!/bin/bash

git clone https://gitlab.com/arunalakmal/satellite-control-system.git
cd satellite-control-system

if [[ ${ACTIVE_PROD} == "blue" ]]; then
  yq e '.spec.template.spec.containers[0].image = "ghcr.io/stefanprodan/podinfo:'$TAG'"' -i prod/application/green/green-deployment.yaml
else
  yq e '.spec.template.spec.containers[0].image = "ghcr.io/stefanprodan/podinfo:'$TAG'"' -i prod/application/blue/blue-deployment.yaml
fi
git config --global user.email "bot@techcrumble.cloud"
git config --global user.name "TechCrumble Bot"

git add -A && git commit -m "Commit by GitBot"
git push https://gitlab-ci-token:${TOKEN}@gitlab.com/arunalakmal/satellite-control-system.git master
